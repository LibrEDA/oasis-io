<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# OASIS Stream Reader / Writer

`libreda-oasis` is a layout input/output module for [LibrEDA](https://libreda.org)

OASIS is a binary file format for chip layouts and a good successor of GDSII.
This library provides code for writing and reading libreda-db layouts to and from OASIS files.

## Documentation

This crate is documented with docstrings in the code.
To view the documentation clone this repository and run: `cargo doc --open`

## Acknowledgements

* Since December 2020 this project is part of libreda.org and hence funded by [NLnet](https://nlnet.nl) and [NGI0](https://nlnet.nl/NGI0).
* The code is heavily inspired from the OASIS implementation of [KLayout](https://klayout.de).
